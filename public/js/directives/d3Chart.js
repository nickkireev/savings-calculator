(function () {
    'use strict';

    angular.module('app.directives')
        .directive('d3Charts', ['d3', function(d3) {
            var MultiGraph = function(elemid,options) {
                this.chart = document.getElementById(elemid);
                this.options = options || {};
                //var data = _data || [];//массив объектов, где каждый объект ver 2.0 {data: [{x:xVal;y:yVal}],color:'#color'}; //{line: [{x:xVal;y:yVal}], dots: [{x:xVal;y:yVal}], color:'#color'};
                var number_slice = this.options.number_slice || -4;// число точек с конца

                var w = parseInt(this.chart.style.width, 10);
                var h = parseInt(this.chart.style.height, 10);

                this.options.xmax = options.xmax || 30;
                this.options.xmin = options.xmin || 0;
                this.options.ymax = options.ymax || 10;
                this.options.ymin = options.ymin || 0;
                this.options.color = options.color || ["#0066cc","#0066cc", "#009933","#009933", "#ff9900","#ff9900", "#ee2222","#ee2222"]

                //************************************************************
                // Create Margins and Axis and hook our zoom function
                //************************************************************
                var margin = {
                        top: 0,
                        right: 30,
                        bottom: 30,
                        left: 50
                    },
                    width = w - margin.left - margin.right,
                    height = h - margin.top - margin.bottom;
                /*width = 960 - margin.left - margin.right,
                 height = 500 - margin.top - margin.bottom;*/

                var x = d3.scale.linear()
                    .domain([this.options.xmin, this.options.xmax])
                    .range([0, width]);

                var y = d3.scale.linear()
                    .domain([this.options.ymin, this.options.ymax])
                    .range([height, 0]);


                var xAxis = d3.svg.axis()
                    .scale(x)
                    .tickSize(-height)
                    .tickPadding(10)
                    .tickSubdivide(true)
                    .ticks(4)
                    .orient("bottom");

                var yAxis = d3.svg.axis()
                        .scale(y)
                        .tickPadding(5)
                        .tickSize(-width)
                        .tickSubdivide(true)
                        .ticks(5)//!!!!!!!!!!!!!!!!!
                        .orient("left")
                        .tickFormat(function (d) {
                            return d !== 0 ? d : '';
                        })
                    ;

                var zoom = d3.behavior.zoom()
                    .x(x)
                    .y(y)
                    .scaleExtent([1, 10])
                    .on("zoom", zoomed);

                function zoomed() {
                    svg.select(".x.axis").call(xAxis);
                    svg.select(".y.axis").call(yAxis);
                    svg.selectAll('path.line').attr('d', lineGen);

                    points.selectAll('circle').attr("transform", function (d) {
                            return "translate(" + x(d.point.x) + "," + y(d.point.y) + ")";
                        }
                    );
                }
                function customAxis(g) {
                    g.selectAll("text")
                        .attr("x", -5)
                        .attr("y", 6);
                }
                //************************************************************
                // Generate our SVG object
                //************************************************************
                var svg = d3.select(this.chart).append("svg")
                        //.call(zoom)
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
                    ;

                svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(xAxis);

                var gy = svg.append("g")
                    .attr("class", "y axis")
                    .call(yAxis)
                    .call(customAxis);

                svg.append("clipPath")
                    .attr("id", "clip")
                    .append("rect")
                    .attr("width", width)
                    .attr("height", height);

                svg.selectAll("text")
                    .attr("y", 7);
                this.redraw_axis = function (val) {
                    y.domain([0, val]);

                    gy.transition()
                        .duration(250)
                        .call(yAxis)
                        .selectAll("text") // cancel transition on customized attributes
                        .tween("attr.x", null)
                        .tween("attr.y", null);

                    gy.call(customAxis);
                }
                this.x = x;
                this.y = y;
                this.gy = y;
                this.svg = svg;
                this.xAxis = xAxis;
                this.yAxis = yAxis;
                this.number_slice = number_slice;
            }
            return {
                restrict: 'EA',
                scope: {
                    data: "=",
                    selected: "=",
                    forecast: "=",
                    label: "@",
                    onClick: "&"
                },
                link: function(scope, iElement, iAttrs) {
                    var colors = ['#0066cc','#009933','#ff9900','#ee2222']
                    colors.indexOf(scope.selected.id);
                    var graph = new MultiGraph("chart1",{
                            "xmax": 800, "xmin": 0,
                            "ymax": 100, "ymin": 0,
                            "title": "Simple Graph1",
                            "number_slice": -4,
                            "color": colors,
                        });
                    // on window resize, re-render d3 canvas
                    /*scope.$watch('selected',function(){
                            return draw(scope.data);
                        });*/
                        /*scope.$watch('data',function(newVals, oldVals) {
                            if(scope.data[0].data[0] !== undefined) {
                                return draw(newVals);
                            }
                        },true);*/

                    scope.$watch('data',function(newVals, oldVals) {
                        var j = 0;
                        newVals.forEach(function(o, i){
                            o.coef == oldVals[i].coef ? j++: j=0;
                        })
                        if(j != 0) {
                            redrawAxis(newVals);
                            draw_dots(scope.forecast, scope.selected);
                            return draw(newVals, scope.selected);
                        }

                    },true);
                    scope.$watch('forecast',function(newVals, oldVals) {
                        draw_dots(newVals, scope.selected);
                    })
                    /*scope.$watch('forecast',function(newVals, oldVals) {
                        console.log('1',newVals[3].selected);
                        return draw_dots(newVals, scope.selected);

                    },true);
                    scope.$watch('forecast',function(newVals, oldVals) {
                        console.log('0',newVals[0].selected);
                        return draw_dots(newVals, scope.selected);

                    });*/
                    function redrawAxis(vals){
                        var max = Math.ceil(d3.max(vals.map(function(obj){return obj.coef;})));
                        if(max > 8)
                            max = Math.ceil((max + max* 0.2) /10)*10;
                        else if(!isNaN(max))
                            max = max + max* 0.3;
                        else
                            max = 100;
                        graph.redraw_axis(max);
                    }
                    function draw(_data){
                        //************************************************************
                        // Create D3 line object and draw data on our SVG object
                        //************************************************************
                        var x = graph.x;
                        var y = graph.y;
                        var data = _data;
                        var svg = graph.svg;
                        var number_slice = graph.number_slice;
                        //console.log('data',data)
                        data.forEach(function (d, i) {
                            //console.log(i % 2);

                            //line[i].push.apply(dots[i]);//рисуем линию и на точках, те добавляем точки в люнию
                            data[i].data = d.data.map(function (obj) {
                                //obj.x must be date
                                //obj.y must be float
                                //var val = 365 - Math.round((new Date() - obj.x) / (1000 * 60 * 60 * 24));//разница дат
                                return {x: obj.x, y: obj.y};
                            })
                        })

                        //----------------------
                        var colors = graph.options.color;
                        data = data.map(function (obj) {
                            if(!obj.selected)
                                return obj.data;
                            else
                                return [];
                        })
                        //----------------------
                        var lineGen = d3.svg.line()
                            //.interpolate("linear")
                            .x(function (d) {
                                //console.log('x(d.x) ',y(d.y), d.x);
                                return x(d.x);
                            })
                            .y(function (d) {
                                //console.log('y(d.y) ',y(d.y), d.y);
                                return y(d.y);
                            });
                        svg.selectAll(".line").remove();
                        svg.selectAll('.line')
                            .data(data)
                            .enter()
                            .append("path")
                            .attr("class", "line")
                            .attr("clip-path", "url(#clip)")//расположение точек
                            .attr('stroke', function (d, i) {
                                return colors[i % colors.length];
                            })
                            .attr("d", lineGen);

                    }
                    function draw_dots(_data){
                        //************************************************************
                        // Create D3 line object and draw data on our SVG object
                        //************************************************************
                        var x = graph.x;
                        var y = graph.y;
                        var data = _data;
                        var svg = graph.svg;
                        var number_slice = graph.number_slice;

                        data.forEach(function (d, i) {
                            //console.log(i % 2);

                            //line[i].push.apply(dots[i]);//рисуем линию и на точках, те добавляем точки в люнию
                            data[i].data = d.data.map(function (obj) {
                                //obj.x must be date
                                //obj.y must be float
                                //var val = 365 - Math.round((new Date() - obj.x) / (1000 * 60 * 60 * 24));//разница дат
                                return {x: obj.x, y: obj.y};
                            })
                        })

                        //----------------------
                        var colors = graph.options.color;
                        var dots = data.map(function (obj) {
                            if(!obj.selected)
                                return obj.data.slice(number_slice);
                            else
                                return [];
                        })

                        data = data.map(function (obj) {
                            if(!obj.selected)
                                return obj.data;
                            else
                                return [];
                        })
                        //----------------------
                        var lineGen = d3.svg.line()
                            //.interpolate("linear")
                            .x(function (d) {
                                //console.log('x(d.x) ',y(d.y), d.x);
                                return x(d.x);
                            })
                            .y(function (d) {
                                //console.log('y(d.y) ',y(d.y), d.y);
                                return y(d.y);
                            });
                        svg.selectAll(".dots").remove();
                        svg.selectAll(".line_forecast").remove();

                        svg.selectAll('.line_forecast')
                            .data(data)
                            .enter()
                            .append("path")
                            .attr("class", "line_forecast")
                            .attr("clip-path", "url(#clip)")//расположение точек
                            .attr('stroke', function (d, i) {
                                return colors[i % colors.length];
                            })
                            .attr("d", lineGen);


                        //************************************************************
                        // Draw points on SVG object based on the data given
                        //************************************************************
                        var drag = d3.behavior.drag()
                            .on("drag", dragmove);
                        var points = svg.selectAll('.dots')
                            .data(dots)
                            .enter()
                            .append("g")
                            .attr("class", "dots")
                            .attr("clip-path", "url(#clip)");

                        points.selectAll('.dot')
                            .data(function (d, index) {
                                var a = [];
                                d.forEach(function (point, i) {
                                    a.push({'index': index, index2: i, 'point': point});
                                });
                                return a;
                            })
                            .enter()
                            .append('circle')
                            .attr('class', 'dot')
                            .attr('class', 'dot')
                            .attr("r", 3)
                            .attr('fill', function (d, i) {
                                return colors[d.index % colors.length];
                            })
                            .attr("transform", function (d) {
                                return "translate(" + x(d.point.x) + "," + y(d.point.y) + ")";
                            })
                            .call(drag)
                            .on("mousedown", function() {d3.event.preventDefault();})
                            .on('mouseup', function (d) { return scope.onClick({item:d}); })
                        //.on("mouseover", function (d) { drag(d); })
                        //.on("mousedown.drag",  function (d) { if (d3.event.defaultPrevented) return; drag() })
                        //.on("touchstart.drag",function (d) { drag() });


                        //************************************************************
                        // drag specific updates
                        //************************************************************
                        function dragmove(d) {
                            //d.index//номер линии
                            //d.index2//номер точки вычисляется неверно, если точки не соответствуют данной линии
                            //data[d.index].length + number_slice + d.index2; //где number_slice количество точек с конца массива
                            //d.point//координаты точки на линии

                            var dssx = data[d.index][d.index2];
                            var dssy = data[d.index];
                            var t = d3.transform(d3.select(this).attr('transform'));
                            //x = t.translate[0] получение координат передвигаемой точки
                            //y = t.translate[1]
                            var new_x =  t.translate[0];//d3.event.x; изменение положения точки только по вертикали
                            var new_y = d3.event.y;

                            d3.select(this).attr("transform", "translate(" + new_x + "," + new_y + ")");
                            data[d.index][data[d.index].length + number_slice + d.index2] = { x:x.invert(new_x),y: y.invert(new_y)};

                            svg.selectAll('path.line_forecast').attr('d', lineGen);
                            //_data[d.index].data[_data[d.index].data.length + number_slice + d.index2] = { x:x.invert(new_x),y: y.invert(new_y)};
                            //console.log(_data);
                            // scope.$apply();
                            //scope.onClick({item:{i: d.index, j: _data[d.index].data.length + number_slice + d.index2, k:{ x:x.invert(new_x),y: y.invert(new_y)}}}); //возвращаем значение при клике в ангуляр
                        }

                    }
                }
            };
        }]);

}());
