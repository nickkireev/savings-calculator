(function () {
  'use strict';

  angular.module('app.directives')
    .directive('d3Bars', ['d3', function(d3) {
          var BarChart = function(elemid, options) {
              this.chart = document.getElementById(elemid);
              this.options = options || {};
              var w = parseInt(this.chart.style.width, 10);
              var h = parseInt(this.chart.style.height, 10);
              var margin = {
                      top: 20,
                      right: 30,
                      bottom: 50,
                      left: 50
                  },
              /*width = 960 - margin.left - margin.right,
               height = 500 - margin.top - margin.bottom;*/
                  width = w - margin.left - margin.right,
                  height = h - margin.top  - margin.bottom;

// Our X scale
              var x = d3.scale.ordinal()
                  .rangeRoundBands([0, width], .1);

// Our Y scale
              var y = d3.scale.linear()
                  .domain([0, 3e6])
                  .rangeRound([height, 0]);

// Use our X scale to set a bottom axis
              var xAxis = d3.svg.axis()
                  .scale(x)
                  .orient("bottom");

              function formatCurrency(d) {
                  return d/1e6;
              }

// Smae for our left axis
              var yAxis = d3.svg.axis()
                  .scale(y)
                  .orient("left")
                  .ticks(5)
                  .tickFormat(formatCurrency);

// Add our chart to the document body
              var svg = d3.select(this.chart).append("svg")
                  .attr("width", width + margin.left + margin.right)
                  .attr("height", height + margin.top + margin.bottom)
                  .append("g")
                  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
              svg.append("g")
                  .attr("class", "x axis")
                  .attr("transform", "translate(0," + height + ")")
                  .call(xAxis);

              svg.append("g")
                  .attr("class", "y axis")
                  .call(yAxis)
                  .append("text")
                  //.attr("transform", "rotate(-90)")
                  .attr("y", -6)
                  .attr("x", 21)
                  .attr("dy", ".71em")
                  .style("text-anchor", "end")
                  .text("млн");


              this.x = x;
              this.y = y;
              this.svg = svg;
              this.xAxis = xAxis;
              this.yAxis = yAxis;
              this.height = height;
              this.width = width;
              this.margin = margin;
              this.color = d3.scale.ordinal();
              svg.append("text")
                  .attr("x", 0 + 10)
                  .attr("y", this.width)
                  //.text(formatNumber(data[0].total ));
                  .text('сегодня');

              svg.append("text")
                  .attr("x", this.height - this.margin.right - 15)
                  .attr("y", this.width )
                  .text('через год');
          };
          BarChart.prototype.draw =  function (_data) {
              var data = _data;
              var x = this.x;
              var y = this.y;
              var svg = this.svg;
              var xAxis = this.xAxis;
              var yAxis = this.yAxis;
              var clr = ["#0066cc","#66a3e0", "#009933","#66c285", "#ff9900","#ffc266", "#ee2222","#ffc266"]//передавать цвета
              //var clr = ["#0066cc", "#009933","#ff9900", "#ee2222"]//передавать цвета
              var color = d3.scale.ordinal()
                  .range(clr);

              // Use our values to set our color bands
              color.domain(d3.keys(_data[0].data[0]));//!!!!

              data.map(function(dt, k){
                  var l1 = [];
                  dt.data.forEach(function (d,i) {
                      var y0 = 0;
                      var mass = [];
                      d3.keys(d).forEach(function (name) {
                          mass.push({
                              name: name,
                              y0: y0,
                              y1: y0 += d[name]
                          });
                      });
                      l1[i] = mass;
                  });
                  dt.types = l1[k];
                  dt.total = dt.types[dt.types.length - 1].y1;
              })
            //console.log('data.types',data);
              // Sort by year
              data.map(function(dt){
                  return dt.data.sort(function (a, b) {
                      return a.number - b.number;
                  });
              });

              // Our X domain is our set of years
              x.domain(data.map(function (d) {
                  return d.number;
              }));

              // Our Y domain is from zero to our highest total
              /*y.domain([0, d3.max(data, function (d) {
               return d.total;
               })]);*/


              /*data draw
               {
               {
               data{ -добавил уровень вложенности
               a: 400
               b: 315
               c: 216
               d: 103
               }
               total: 1034
               types: [
               {
               name: "a"//имя это для цвета.  тоесть определяем цвет по имени рисуя прямоугольник получаем цвет соответственно имени
               y0: 0
               y1: 400
               }
               },
               {
               name: "b"
               y0: 400
               y1: 715
               }
               ]
               number: 1 //номер прямоугольника
               }
               }
               */
              svg.selectAll("rect").remove();
              var year = svg.selectAll(".year")
                  .data(data)
                  .enter().append("g")
                  .attr("class", "g")
                  .attr("transform", function (d) {
                      return "translate(" + x(d.number) + ",0)";
                  });


              year.selectAll("rect")
                  .data(function (d) {
                      return d.types;// Для отрисовки передаём массивы, отобранные  у data (types) объект {имя ,  координаты 1 , коордиинаты 2}
                  })
                  .enter().append("rect")
                  .attr("width", x.rangeBand())
                  .attr("y", function (d) {
                      return y(d.y1);
                  })
                  .attr("height", function (d) {
                      //console.log( y(d.y1),y(d.y0), d.y0, d.y1)
                      return y(d.y0) - y(d.y1);
                  })
                  .style("fill", function (d) {
                      //передаём в цвет имячко то, какое у этого прямоугольника => заполняет нужными цветами
                      return color(d.name);
                  });
              svg.selectAll(".after").remove();
              svg.selectAll(".now").remove();
              var formatNumber = function(d){
                  var frmttd = d3.format(",")(Math.floor(d));
                  return frmttd.replace(","," ");
              }

              svg
                  .append("text")
                  .attr("x", 0 + 10)
                  .attr("y", this.width + 20)
                  .attr("class","now")
                  .text(formatNumber(data[0].total))
              svg
                  .append("text")
                  .attr("x", this.height - this.margin.right - 15)
                  .attr("y", this.width + 20)
                  .attr("class","after")
                  .text(formatNumber(data[4].total))




          };
      return {
        restrict: 'EA',
        scope: {
          data: "=",
          forecast: "=",
          changed: "=",
          pers: "=",
          label: "@",
          onClick: "&"
        },
        link: function(scope, iElement, iAttrs) {
            var bar =  new BarChart("bar1",  {
                "xmax": 365*2, "xmin": 0,
                "ymax": 100, "ymin": 0,
                "title": "Simple Graph1",
                "xlabel": "X Axis",
                "ylabel": "Y Axis"
            });

          // on window resize, re-render d3 canvas
          scope.$watch('pers',function(){
              //console.log('pers',scope.data)
              //console.log('forecast',scope.forecast)
              if(scope.data !== undefined && scope.forecast.length != 0) {
                  return scope.render(scope.data,scope.forecast);
              }
              },true);

          // watch for data changes and re-render
          scope.$watch('data', function(newVals, oldVals) {

              //console.log('pers',scope.data)
              //console.log('forecast',scope.forecast)
              if(newVals[0].data[0] !== undefined && scope.forecast.length != 0) {
                  return scope.render(newVals,scope.forecast);
              }
          },true);
          scope.$watch('changed', function(newVals, oldVals) {
              if(scope.data !== undefined && scope.forecast.length != 0) {
                  return scope.render(scope.data,scope.forecast);
              }
          });
          scope.$watch('forecast', function(newVals, oldVals) {

              //console.log('nv ov ',newVals,oldVals)

              if(newVals.length != 0) {
                  //console.log('forecast',scope.forecast[0].data[4])
                  return scope.render(scope.data,newVals);
              }
          },true);

          // define render function
          scope.render = function(data,forecast){
             var d  =  getBarData(data,forecast)
              bar.draw(d);
            //console.log('render data ',data);
            //console.log('bar ',bar);
          };
            function getBarData(_data,_forecast){
                var data =[];
                var pers =[];
                _data.forEach(function(d, i){
                    var item = [] ;// data - {1: 250000, 2: 250000, 3: 250000,4: 250000} прогноз значения каждой-валюты; 0 - сегодня, 1 - через квартал,2,3,4
                    _forecast[i].data.forEach(function(obj, j){
                        item[j] = division(d.val, obj.y);//{1: 250000, 2: 250000, 3: 250000,4: 250000} ->{1: 250000, 2: 260000, 3: 270000,4: 280000}
                        //console.log('division',division(d.val, d.coef));
                    })
                    data.push(item);
                })
                _data.forEach(function(d, i){
                    var item = [] ;// data - {1: 250000, 2: 250000, 3: 250000,4: 250000} прогноз значения каждой-валюты; 0 - сегодня, 1 - через квартал,2,3,4
                    var len = _forecast[i].data.length;
                    _forecast[i].data.forEach(function(obj, j){
                        if(scope.pers) {
                            //console.log(len, j);
                            if (j == 0) {
                                item[j] = 0;//{1: 15, 2: 15, 3: 15,4: 15} ->{1: 250000, 2: 260000, 3: 270000,4: 280000}
                            } else {
                                item[j] = d.pers / len;
                                //console.log('d.pers', d.pers)
                            }
                            len--;
                        }
                        else
                            item[j] = 0;
                    })
                    pers.push(item);
                })
                //console.log('data',data)
                var dta = [];/*[{ number:1, data: [ 0: 250000 1: 10000 2: 8000 3: 6000],[]},{...}]*/
                var tdata = transpMatrix(data,pers);//[[],[]]

                //console.log('tdata',tdata)
                for(var i = 0; i<tdata.length; i++) {
                    var item ={number: i, data:[]};
                    item['data'] = tdata;
                    dta.push(item);
                }
                //console.log('finale data',dta)
                return dta;
            }
            function division(a,b){
                //return a * b;
                return Math.floor(a * b);
            }
            function transpMatrix(A, pers){
                var m = A.length, n = A[0].length, AT = [];
                for (var i = 0; i < n; i++)
                {   var k=0;
                    AT[i] = [];
                    for (var j = 0; j < m; j++) {
                        AT[i][j+k] = A[j][i]; k++;
                            AT[i][j + k] = A[j][i] * 0.01 * pers[j][i];//проценты

                    }
                }
                return AT;
            }
        }
      };
    }]);

}());
