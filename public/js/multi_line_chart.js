MultiGraph = function(elemid,_data,options) {
    this.chart = document.getElementById(elemid);
    this.options = options || {};
    var data = _data || [];//массив объектов, где каждый объект ver 2.0 {data: [{x:xVal;y:yVal}],color:'#color'}; //{line: [{x:xVal;y:yVal}], dots: [{x:xVal;y:yVal}], color:'#color'};
    var number_slice = this.options.number_slice || -4;// число точек с конца
    if (!data) {
        data = [{data: [{x: 0, y: 2}, {x: 0, y: 4}, {x: 0, y: 6},], color: '#000000'}];
    }
    var w = parseInt(this.chart.style.width, 10);
    var h = parseInt(this.chart.style.height, 10);

    this.options.xmax = options.xmax || 30;
    this.options.xmin = options.xmin || 0;
    this.options.ymax = options.ymax || 10;
    this.options.ymin = options.ymin || 0;

    //var colors =["#0066cc","#0066cc", "#009933","#009933", "#ff9900","#ff9900", "#ee2222","#ee2222"]

//************************************************************
// Create Margins and Axis and hook our zoom function
//************************************************************
    var margin = {
            top: 0,
            right: 30,
            bottom: 30,
            left: 50
        },
        width = w - margin.left - margin.right,
        height = h - margin.top - margin.bottom;
    /*width = 960 - margin.left - margin.right,
     height = 500 - margin.top - margin.bottom;*/

    var x = d3.scale.linear()
        .domain([this.options.xmin, this.options.xmax])
        .range([0, width]);

    var y = d3.scale.linear()
        .domain([this.options.ymin, this.options.ymax])
        .range([height, 0]);


    var xAxis = d3.svg.axis()
        .scale(x)
        .tickSize(-height)
        .tickPadding(10)
        .tickSubdivide(true)
        .ticks(4)
        .orient("bottom");

    var yAxis = d3.svg.axis()
            .scale(y)
            .tickPadding(5)
            .tickSize(-width)
            .tickSubdivide(true)
            .ticks(5)//!!!!!!!!!!!!!!!!!
            .orient("left")
            .tickFormat(function (d) {
                return d !== 0 ? d : '';
            })
        ;

    var zoom = d3.behavior.zoom()
        .x(x)
        .y(y)
        .scaleExtent([1, 10])
        .on("zoom", zoomed);

    function zoomed() {
        svg.select(".x.axis").call(xAxis);
        svg.select(".y.axis").call(yAxis);
        svg.selectAll('path.line').attr('d', lineGen);

        points.selectAll('circle').attr("transform", function (d) {
                return "translate(" + x(d.point.x) + "," + y(d.point.y) + ")";
            }
        );
    }
//************************************************************
// Generate our SVG object
//************************************************************
    var svg = d3.select(this.chart).append("svg")
        //.call(zoom)
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        ;

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

    svg.append("g")
        .attr("class", "y axis")
        .append("text")
        .attr("class", "axis-label")
        .attr("transform", "rotate(-90)")
        .attr("y", (-margin.left) + 10)
        .attr("x", -height / 2)
        //.text('Axis Label');

    svg.append("clipPath")
        .attr("id", "clip")
        .append("rect")
        .attr("width", width)
        .attr("height", height);

    svg.selectAll("text")
        .attr("y", 7);
    this.x = x;
    this.y = y;
    this.svg = svg;
    this.xAxis = xAxis;
    this.yAxis = yAxis;
    this.number_slice = number_slice;
}
    //перенес в d3Chart
MultiGraph.prototype.draw = function(_data){
//************************************************************
// Create D3 line object and draw data on our SVG object
//************************************************************
    var x = this.x;
    var y = this.y;
    var data = _data;
    var svg = this.svg;
    var number_slice = this.number_slice;

    data.forEach(function (d, i) {
        //console.log(i % 2);

        //line[i].push.apply(dots[i]);//рисуем линию и на точках, те добавляем точки в люнию
        data[i].data = d.data.map(function (obj) {
            //obj.x must be date
            //obj.y must be float
            //var val = 365 - Math.round((new Date() - obj.x) / (1000 * 60 * 60 * 24));//разница дат
            return {x: obj.x, y: obj.y};
        })
    })

    //----------------------
    var colors = data.map(function (obj) {
        return obj.color;
    })
    var dots = data.map(function (obj) {
        return obj.data.slice(number_slice);
    })
    data = data.map(function (obj) {
        return obj.data;
    })
    //----------------------
    var lineGen = d3.svg.line()
        .interpolate("linear")
        .x(function (d) {
            return x(d.x);
        })
        .y(function (d) {
            return y(d.y);
        });

    svg.selectAll('.line')
        .data(data)
        .enter()
        .append("path")
        .attr("class", "line")
        .attr("clip-path", "url(#clip)")//расположение точек
        .attr('stroke', function (d, i) {
            return colors[i % colors.length];
        })
        .attr("d", lineGen);


//************************************************************
// Draw points on SVG object based on the data given
//************************************************************
    var drag = d3.behavior.drag()

        //.on("click", function(d, i){return scope.onClick({item: d});})
        .on("drag", dragmove);
    var points = svg.selectAll('.dots')
        .data(dots)
        .enter()
        .append("g")
        .attr("class", "dots")
        .attr("clip-path", "url(#clip)");

    points.selectAll('.dot')
        .data(function (d, index) {
            var a = [];
            d.forEach(function (point, i) {
                a.push({'index': index, index2: i, 'point': point});
            });
            return a;
        })
        .enter()
        .append('circle')
        .attr('class', 'dot')
        .attr('class', 'dot')
        .attr("r", 2.5)
        .attr('fill', function (d, i) {
            return colors[d.index % colors.length];
            })
        .attr("transform", function (d) {
            return "translate(" + x(d.point.x) + "," + y(d.point.y) + ")";
            })

        .call(drag);
        //.on("mouseover", function (d) { drag(d); })
        //.on("mousedown.drag",  function (d) { drag() })
        //.on("touchstart.drag",function (d) { drag() });


//************************************************************
// Zoom specific updates
//************************************************************

     function dragmove(d) {

        //d.index//номер линии
        //d.index2//номер точки вычисляется неверно, если точки не соответствуют данной линии
        //data[d.index].length + number_slice + d.index2; //где number_slice количество точек с конца массива
        //d.point//координаты точки на линии

        var dssx = data[d.index][d.index2];
        var dssy = data[d.index];

        var t = d3.transform(d3.select(this).attr('transform'));
        //x = t.translate[0] получение координат передвигаемой точки
        //y = t.translate[1]
        var new_x =  t.translate[0];//d3.event.x; изменение положения точки только по вертикали
        var new_y = d3.event.y;

        d3.select(this).attr("transform", "translate(" + new_x + "," + new_y + ")");
        data[d.index][data[d.index].length + number_slice + d.index2] = { x:x.invert(new_x),y: y.invert(new_y)};
        svg.selectAll('path.line').attr('d', lineGen);

        _data[d.index].forecast[_data[d.index].data.length + number_slice + d.index2] = { x:x.invert(new_x),y: y.invert(new_y)};

         //return scope.onClick(d); возвращаем значение при клике в ангуляр

    }

}