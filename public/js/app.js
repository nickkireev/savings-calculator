(function () {
    'use strict';

    // create the angular app
    var app = angular.module('app', [
        'app.controllers',
        'app.directives'
    ]).filter('to_trusted', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml(text);
        }
    }]);

    // setup dependency injection
    angular.module('d3', []);
    angular.module('app.controllers', [])
    angular.module('app.directives', ['d3']);


}());