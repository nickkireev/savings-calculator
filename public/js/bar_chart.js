BarChart = function(elemid, options) {
    this.chart = document.getElementById(elemid);
    this.options = options || {};
    var w = parseInt(this.chart.style.width, 10);
    var h = parseInt(this.chart.style.height, 10);
    var margin = {
            top: 20,
            right: 30,
            bottom: 30,
            left: 50
        },
        /*width = 960 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;*/
        width = w - margin.left - margin.right,
        height = h - margin.top  - margin.bottom;

// Our X scale
    var x = d3.scale.ordinal()
        .rangeRoundBands([0, width], .1);

// Our Y scale
    var y = d3.scale.linear()
        .domain([0, 3e6])
        .rangeRound([height, 0]);

// Use our X scale to set a bottom axis
    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    function formatCurrency(d) {
        return d/1e6;
    }
// Smae for our left axis
    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .ticks(5)
        .tickFormat(formatCurrency);

// Add our chart to the document body
    var svg = d3.select(this.chart).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        //.attr("transform", "rotate(-90)")
        .attr("y", -6)
        .attr("x", 21)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("млн");

    this.x = x;
    this.y = y;
    this.svg = svg;
    this.xAxis = xAxis;
    this.yAxis = yAxis;
    this.color = d3.scale.ordinal();
};
BarChart.prototype.draw =  function (_data) {
    var data = _data;
    var x = this.x;
    var y = this.y;
    var svg = this.svg;
    var xAxis = this.xAxis;
    var yAxis = this.yAxis;
    var clr = ["#0066cc", "#009933", "#ff9900", "#ee2222"]//передавать цвета
    var color = d3.scale.ordinal()
        .range(clr);

    // Use our values to set our color bands
    color.domain(d3.keys(data[0].data[0]));//!!!!

        data.map(function(dt, k){
            var l1 = [];
            dt.data.forEach(function (d,i) {
                var y0 = 0;
                var mass = [];
                d3.keys(d).forEach(function (name) {
                    mass.push({
                        name: name,
                        y0: y0,
                        y1: y0 += d[name]
                    });
                });
                l1[i] = mass;
            });
            dt.types = l1[k];
            dt.total = dt.types[dt.types.length - 1].y1;
        })

        // Sort by year
        data.map(function(dt){
            return dt.data.sort(function (a, b) {
                return a.number - b.number;
            });
        });

        // Our X domain is our set of years
        x.domain(data.map(function (d) {
            return d.number;
        }));

        // Our Y domain is from zero to our highest total
        /*y.domain([0, d3.max(data, function (d) {
            return d.total;
        })]);*/


            /*data draw
             {
                 {
                 data{ -добавил уровень вложенности
                 a: 400
                 b: 315
                 c: 216
                 d: 103
                 }
                 total: 1034
                 types: [
                         {
                             name: "a"//имя это для цвета.  тоесть определяем цвет по имени рисуя прямоугольник получаем цвет соответственно имени
                             y0: 0
                             y1: 400
                             }
                         },
                         {
                             name: "b"
                             y0: 400
                             y1: 715
                         }
                    ]
                number: 1 //номер прямоугольника
                }
             }
             */
        svg.selectAll("rect").remove();
        var year = svg.selectAll(".year")
            .data(data)
            .enter().append("g")
            .attr("class", "g")
            .attr("transform", function (d) {
                return "translate(" + x(d.number) + ",0)";
            });


        year.selectAll("rect")
            .data(function (d) {
                return d.types;// Для отрисовки передаём массивы, отобранные  у data (types) объект {имя ,  координаты 1 , коордиинаты 2}
            })
            .enter().append("rect")
            .attr("width", x.rangeBand())
            .attr("y", function (d) {
                return y(d.y1);
            })
            .attr("height", function (d) {
                //console.log( y(d.y1),y(d.y0), d.y0, d.y1)
                return y(d.y0) - y(d.y1);
            })
            .style("fill", function (d) {
                //передаём в цвет имячко то, какое у этого прямоугольника => заполняет нужными цветами
                return color(d.name);
            });

};