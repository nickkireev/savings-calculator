(function () {
  'use strict';

  angular.module('app.controllers')
    .controller('LineCtrl', ['$scope', '$http', '$templateCache', '$sce','$timeout','$rootScope', function($scope,$http,$templateCache,$sce,$timeout,$rootScope) {
          $scope.d3Data = [
              {
                  id: '0',
                  icon: /*'fa-rub'*/  '&#xf158;',
                  alias: 'RUB',
                  val: '250000',
                  coef: 1,
                  pers: '10',
                  data: [],
                  forecast: [],
                  color: '#0066cc',
                  selected: true
              },
              {
                  id: '1',
                  icon: /*'fa-usd'*/  '&#xf155;',
                  alias: 'USD',
                  val: '10000',
                  coef: 75.86,
                  pers: '4',
                  data: [],
                  forecast:[],
                  color: '#009933',
                  selected: false
              },
              {
                  id: '2',
                  icon: /*'fa-eur'*/  '&#xf153;',
                  alias: "EUR",
                  val: '8000',
                  coef: 82.85,
                  pers: '3',
                  data: [],
                  forecast: [],
                  color: '#ff9900',
                  selected: false
              },
              {
                  id: '3',
                  icon: /*'fa-jap'*/  '&#xf157;',
                  alias: "CNY",
                  val: '6000',
                  coef: 11.53,
                  pers: '2',
                  data: [],
                  forecast: [],
                  color: '#ee2222',
                  selected: false
              }

          ];
          $scope.forecast = [];
          $scope.cng = 1;
          $scope.colors = ['#0066cc','#009933','#ff9900','#ee2222']
          $scope.d3OnDrag = function(item){
              //console.log(item,$scope.forecast);
              $scope.$apply();
          };
          $scope.checked = false;

          $scope.extendCurrency = [
              {
                  id: '5', icon: '&nbsp;', alias: "AUD", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '6', icon: '&nbsp;', alias: "BGN", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '7', icon: '&nbsp;', alias: "BRL", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '8', icon: '&nbsp;', alias: "CAD", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '9', icon: '&nbsp;', alias: "CHF", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '10', icon: '&nbsp;', alias: "CZK", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '11', icon: '&nbsp;', alias: "DKK", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '12', icon: '&#xf154;', alias: "GBP", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '13', icon: '&nbsp;', alias: "HKD", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '14', icon: '&nbsp;', alias: "HRK", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '15', icon: '&nbsp;', alias: "HUF", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '16', icon: '&nbsp;', alias: "IDR", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '17', icon: '&nbsp;', alias: "ILS", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '18', icon: '&nbsp;', alias: "INR", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '19', icon: '&nbsp;', alias: "JPY", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '20', icon: '&nbsp;', alias: "KRW", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '21', icon: '&nbsp;', alias: "LTL", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '22', icon: '&nbsp;', alias: "MXN", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '23', icon: '&nbsp;', alias: "MYR", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '24', icon: '&nbsp;', alias: "NOK", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '25', icon: '&nbsp;', alias: "NZD", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '26', icon: '&nbsp;', alias: "PHP", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '27', icon: '&nbsp;', alias: "PLN", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '28', icon: '&nbsp;', alias: "RON", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '29', icon: '&nbsp;', alias: "SEK", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '30', icon: '&nbsp;', alias: "SGD", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '31', icon: '&nbsp;', alias: "THB", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '32', icon: '&nbsp;', alias: "TRY", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },{
                  id: '33', icon: '&nbsp;', alias: "ZAR", val: '100', coef: '1', pers: '2', color:  function () {return getRandomColor()} },
          ];


          $scope.makeOptions = function() {
              $scope.d3Data.forEach(function(d,i){
                  d.icon =  htmlDecode(d.icon);
              })
              return $scope.d3Data;
          };
          $scope.currency = $scope.value = $scope.d3Data[0];
          update();
          $scope.onValueChange = function () {
              $scope.currency = $scope.value;
              var arr = update();
              //$scope.d3Data = arr[0];
              //$scope.forecast = arr[1];
              /*$scope.d3Data.forEach(function(obj){
                  if($scope.currency.id = obj.id)
                    obj.selected = false;
                  else
                      obj.selected = true;
              })*/
          }

          var i = 0;
          $scope.addCurrency = function () {
              if ($scope.extendCurrency[i]) {
                  $scope.d3Data.push($scope.extendCurrency[i]);
                  update();
                  i++;
              }
          }
          $scope.division = function (a,b){
              //return a * b;
              return Math.floor(a * b);
          }
          $scope.visible = function (obj) {
              var label = '';
              if (obj.icon == '') {
                  label = obj.alias;
              }
              return label;
          }
          function  htmlDecode(input){
              var e = document.createElement('div');
              e.innerHTML = input;
              return e.childNodes[0].nodeValue;
          }
          function getRandomColor() {
              var letters = '0123456789ABCDEF'.split('');
              var color = '#';
              for (var i = 0; i < 6; i++) {
                  color += letters[Math.floor(Math.random() * 16)];
              }
              return color;
          }

          function update() {
              var  base_currency = $scope.currency.alias;
              var currencies_array = $scope.d3Data.map(function (obj) {
                  return obj.alias;
              });
              var url = getCurrencyData(base_currency, currencies_array)
              var collection = $scope.d3Data;

              $http.get('data/' + base_currency + '_2015-01-21_2016-01-21.json').success(function (data) {
                  $scope.d3Data.forEach(function (point, i) {
                      var arr = [];
                      angular.forEach(data, function (value, key) {
                          if (typeof value.rates != 'undefined') {
                              var value = parseFloat(value.rates[point.alias]);
                              arr.push({x: dataConvert(new Date(Date.parse(key))), y: !isNaN(value) ? 1/value : 1});
                          }
                      })
                      point.selected = (point.alias == base_currency)//изменение d3Data
                      $scope.forecast[i].selected = (point.alias == base_currency);
                      collection[i].data = arr;
                  })

              })
                  .error(function (data, status, headers, config) {
                      console.log('err');
                  })

              $http.get(url).success(function (response) {
                  //console.log(response.rates.CNY);
                  collection.forEach(function (point, i) {
                      var value = parseFloat(response.rates[point.alias])
                      collection[i].coef = !isNaN(value) ? 1/value : 1;
                  })
              })
                  .error(function (data, status, headers, config) {
                      console.log(data);
                  });
              $timeout(function(){
                  $scope.forecast = setForecast(collection);
                  $scope.d3Data = collection;
              },300)
              //return [ collection, setForecast(collection)]
          };

          function getCurrencyData(_base_curr, _currencies_array, _date) {
              /*
               * получение валют
               *
               * возвращает ссылку для получения коэффициента в той или иной валюте
               * [{}] где _base_curr - строка базовой валюты, например 'USD'
               * _currencies_array - массив из валют, в которые конвертировать ['RUB','EUR']
               * _date - дата выгрузки значений в формате '2016-10-01', если пустое - отдаст за последний день
               * */

              var date = 'latest';
              var base = '';
              var symbols = '';

              if (_date) {
                  date = _date;
              }
              if (Array.isArray(_currencies_array) && _currencies_array.length != 0) {
                  symbols = '&symbols=' + _currencies_array.join(',');
              }
              if (_base_curr != '' && _base_curr != 'EUR') {
                  base = '?base=' + _base_curr;
              } else {
                  symbols = '';//'?'+symbols.slice(1);
              }
              return 'http://api.fixer.io/' + date + base + symbols;
          }
          function dataConvert(date){
              return 365 - Math.ceil((new Date() - date) / (1000 * 60 * 60 * 24));//разница дат
          }
          function setForecast(coll) {
              /*
               * рисование графиков
               *
               * daily_val - массив массивов [[],[]] для построения грффиков внутренние массивы - {x:date;y:val} координаты
               *
               * */
              var frcst = [];
              coll.forEach(function (obj,i) {
                  var coef = obj.coef;
                  var n = 4;
                  var forecast = {selected: obj.selected, data:[]};// прогнозируемые точки
                  for (var j = 0; j < n + 1; j++) {
                      var c_currency_time = new Date();
                      var xter = dataConvert(new Date(c_currency_time.setDate(c_currency_time.getDate() + (795 - 365) / n * j)));
                      var yter = coef;//daily_val[i - 1].y;//последний известный курс данной валюты
                      forecast.data.push({x: xter, y: yter});
                  }
                  frcst.push(forecast);
              })
              return frcst;
          }
      }]);

}());
